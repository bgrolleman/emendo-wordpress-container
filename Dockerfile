FROM wordpress:php7.2-apache
RUN apt update \
    && apt install -y  \
         unzip \
         wget \
         less \
    && apt clean

# Plugins
COPY scripts/wp_install_plugin /usr/local/bin

ENV BASE_PLUGINS="google-sitemap-generator iwp-client google-analytics-dashboard-for-wp contact-form-7 w3-total-cache wp-fail2ban wordfence cleantalk-spam-protect"
RUN wp_install_plugin $BASE_PLUGINS

# WP CLI
RUN wget -qO /bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && chmod 755 /bin/wp

copy scripts/emendo-entrypoint /usr/local/bin
ENTRYPOINT ["emendo-entrypoint"]
CMD ["apache2-foreground"]

