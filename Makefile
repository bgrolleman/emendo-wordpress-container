help:
	@echo "Emendo Docker Container Makefile"
	@echo
	@echo "build   Create container image"
	@echo "test    Run shell in container image"
	@echo "clean   Remove all data, carefull"
	@echo "start   Run local mysql/wordpress/phpmyadmin/nginx setup"

build:
	docker build -t emendo_wordpress .

test:
	docker run -ti --rm emendo_wordpress bash

clean:
	sudo rm -Ir .data

start:
	docker-compose up --build
